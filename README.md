# hipthread

在no-std环境下，封装unix的pthread和mingw的winpthread, 可支持unix和windows.

## v0.1.3版本更新

消除只支持`***-pc-windows-gnu`的约束，也可以支持`***-pc-windows-msvc`, 只是运行时依赖`mingw`的`libwinpthread-1.dll`.

## v0.1.2版本功能
1. 新增`LocalKey<T>`: 封装`ThrdLocal`,更方便使用

```rust
static KEY: LocalKey<i32> = LocalKey::new();

fn main() {
    assert!(KEY.get().is_null());
    KEY.set(&101);
    spawn(|| {
        assert!(KEY.get().is_null());
        KEY.set(&102);
    }).unwrap().join();
    assert_eq!(KEY.replace(core::ptr::null()), &101);
}
 
```

## v0.1.1版本功能
1. 新增`LocalRefCell<T>`: 提供`RefCell<T>`类型的TLS变量操作接口
1. 新增`LocalCell<T>`: 提供`Cell<T>`类型的TLS变量操作接口

```rust
static LOCAL: LocalCell<i32> = LocalCell::new(|| 100);

fn main() {
    assert_eq!(LOCAL.get(), 100); 
    LOCAL.replace(200);
    let handle = spawn(|| {
        LOCAL.replace(300);
        LOCAL.get()
    }).unwrap();
    assert_eq!(handle.join().unwrap(), 300);
    assert_eq!(LOCAL.get(), 200);
}
```

## v0.1.0版本功能
1. `spawn/spawn_with`: 创建线程
1. `ThrdLocal`: TLS变量的存取.
1. `Mutex`: 互斥锁
1. `Once/OnceLock/LazyLock`: 类似std库中的同名类功能.
1. `sched_cpu_count`: 获取当前进程可用的核数量.
1. `sched_getaffinity/sched_setaffinity`: 设置当前进程同cpu核的亲和性
1. `thrd_setaffinity`: 设置当前线程同cpu核的亲和性.
1. `thrd_setname/thrd_getname`: 设置获取当前线程的名字，方便调测.
